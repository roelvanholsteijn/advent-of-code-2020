package helpers

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func ReadFile(fileName string) []string {
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}

	lines := strings.Split(string(content), "\n")

	return lines
}

func ConvertStringArrayToIntArray(sArray []string) []int {
	var iArray []int

	for _, i := range sArray{
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		iArray = append(iArray, j)
	}

	return iArray
}

func PrintOutput(partNumber int, value interface{}) {
	fmt.Print("_____\n")
	fmt.Printf("Part %d:\n", partNumber)
	fmt.Print(value)
	fmt.Print("\n_____\n")
}