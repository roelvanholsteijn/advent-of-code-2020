package main

import (
	. "../helpers"
	"strconv"
	"strings"
)

type password struct {
	firstNum int
	secondNum int
	requiredLetter string
	password string
}

func formatPasswords(lines []string) []password {
	var passwords []password
	
	// By totally intentionally not removing the space before the password I can use the numbers as index. 
	for _, i := range lines {
		pass := strings.Split(i, ":")
		pass1 := strings.Split(pass[0], "-")
		pass2 := strings.Split(pass1[1], " ")
		firstNum, _ := strconv.Atoi(pass1[0])
		secondNum, _ := strconv.Atoi(pass2[0])

		p := password{firstNum, secondNum, pass2[1], pass[1]}
		passwords = append(passwords, p)
	}

	return passwords
}

func (p password) isValidPasswordPart1() bool {
	letterCount := strings.Count(p.password, p.requiredLetter)

	return letterCount <= p.secondNum && letterCount >= p.firstNum
}

func (p password) isValidPasswordPart2() bool {
	b := []byte(p.requiredLetter)

	return (p.password[p.firstNum] == b[0] && p.password[p.secondNum] != b[0]) ||
		(p.password[p.firstNum] != b[0] && p.password[p.secondNum] == b[0])
}

func part1(p []password)  {
	count := 0
	for _, pass := range p {
		if pass.isValidPasswordPart1() {
			count++
		}
	}

	PrintOutput(1, count)
}

func part2(p []password)  {
	count := 0
	for _, pass := range p {
		if pass.isValidPasswordPart2(){
			count++
		}
	}


	PrintOutput(2, count)
}

func main() {
	lines := ReadFile("./day-2/input.txt")
	passwords := formatPasswords(lines)

	part1(passwords)
	part2(passwords)
}
