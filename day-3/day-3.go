package main

import (
	. "../helpers"
)

const TREE_CHAR = '#'

type slope struct {
	slope []string
	x int
	y int
}

func (s slope) isTree() bool {
	return s.slope[s.x][s.y] == TREE_CHAR
}

func countTrees(s slope, moveRight int, moveDown int) int {
	trees := 0
	sLength := len(s.slope[0])

	for i := 0; i < len(s.slope); i += moveDown {
		if s.isTree() {
			trees++
		}

		s.x += moveDown
		s.y += moveRight

		if s.y >= sLength {
			s.y = s.y % sLength
		}
	}

	return trees
}

func main() {
	lines := ReadFile("./day-3/input.txt")

	s := slope{lines, 0, 0}

	PrintOutput(1, countTrees(s, 3,1))
	PrintOutput(2,
		countTrees(s, 1,1) *
		countTrees(s, 3,1) *
		countTrees(s, 5,1) *
		countTrees(s, 7,1) *
		countTrees(s, 1,2))
}