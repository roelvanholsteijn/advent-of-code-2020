package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

var VALID_HAIR_COLORS = []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}

func main() {
	lines := importFile("./day-4/input.txt")

	var passports []passport

	for _, i := range lines {
		p := passport{}

		i = strings.Replace(i, "\n", " ", -1)
		keyValuePairs := strings.Split(i, " ")
		for _, j := range keyValuePairs{
			pair := strings.Split(j, ":")

			switch pair[0] {
			case "byr":
				p.birthYear, _ = strconv.Atoi(pair[1])
			case "iyr":
				p.issueYear, _ = strconv.Atoi(pair[1])
			case "eyr":
				p.expirationYear, _ = strconv.Atoi(pair[1])
			case "hgt":
				p.height = pair[1]
			case "hcl":
				p.hairColor = pair[1]
			case "ecl":
				p.eyeColor = pair[1]
			case "pid":
				p.passportId = pair[1]
			}
		}
		passports = append(passports, p)
	}
	valid := 0
	for _, p := range passports {
		if p.isValid() {
			valid++
		}
	}

	fmt.Printf("Part 2: %d\n", valid)
}

func importFile(fileName string) []string{
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}
	lines := strings.Split(string(content), "\n\n")

	return lines
}


type passport struct{
	birthYear int
	issueYear int
	expirationYear int
	height string
	hairColor string
	eyeColor string
	passportId string
}

func (p passport) isValid() bool{
	return p.birthYearIsValid() && p.issueYearIsValid() && p.expirationYearIsValid() &&
		p.heightIsValid() && p.hairColorIsValid() && p.eyeColorIsValid() && p.passportIdIsValid()
}

func (p passport) birthYearIsValid() bool {
	return isBetween(p.birthYear, 1920, 2002)
}

func (p passport) issueYearIsValid() bool {
	return isBetween(p.issueYear, 2010, 2020)
}

func (p passport) expirationYearIsValid() bool {
	return isBetween(p.expirationYear, 2020, 2030)
}

func (p passport) heightIsValid() bool {
	if len(p.height) < 3 {
		return false
	}
	
	unit := p.height[len(p.height)-2:]
	height, _ := strconv.Atoi(p.height[:len(p.height)-2])

	if (unit == "cm" && isBetween(height, 150, 193) ) ||
		(unit == "in" && isBetween(height, 59, 76)){
		return true
	} else {
		return false
	}
}

func (p passport) hairColorIsValid() bool {
	re, _ := regexp.Compile("^#([A-Fa-f0-9]{6})")

	return re.MatchString(p.hairColor) && len(p.hairColor) == 7
}

func (p passport) eyeColorIsValid() bool {
	for _, i := range VALID_HAIR_COLORS{
		if p.eyeColor == i {
			return true
		}
	}
	return false
}

func (p passport) passportIdIsValid() bool {
	_, err := strconv.Atoi(p.passportId);
	return err == nil && len(p.passportId) == 9
}

func isBetween(value int, min int, max int) bool {
	return  value >= min && value <= max
}